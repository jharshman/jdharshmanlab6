package com.example.jharshman.jdharshmanlab6;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        onSharedPreferenceChanged(sharedPreferences, "Name");
        onSharedPreferenceChanged(sharedPreferences, "Years");
        onSharedPreferenceChanged(sharedPreferences, "home_world");

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        String[] homeWorlds;
        Preference prf;

        if(key.equalsIgnoreCase("name")) {
            prf = findPreference(key);
            prf.setSummary(sharedPreferences.getString(key, ""));
        } else if(key.equalsIgnoreCase("Years")) {
            prf = findPreference(key);
            prf.setSummary(sharedPreferences.getString(key, "0"));
        } else if(key.equalsIgnoreCase("home_world")) {
            homeWorlds = getResources().getStringArray(R.array.homeWorlds);
            prf = findPreference(key);
            prf.setSummary(homeWorlds[Integer.parseInt(sharedPreferences.getString(key,"0"))]);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

}
