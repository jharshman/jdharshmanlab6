package com.example.jharshman.jdharshmanlab6;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private TextView mGreeting;
    private TextView mWelcome;
    private TextView mSuggestion;
    private ImageView mBackground;

    private int[] images = {
            R.drawable.academy,
            R.drawable.andoria,
            R.drawable.bajor,
            R.drawable.betazed,
            R.drawable.cardassia,
            R.drawable.denobula,
            R.drawable.earth,
            R.drawable.ferenginar,
            R.drawable.fluidic,
            R.drawable.kronos,
            R.drawable.remus,
            R.drawable.romulus,
            R.drawable.suliban,
            R.drawable.talax,
            R.drawable.talos
    };

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mGreeting = (TextView)v.findViewById(R.id.greeting);
        mWelcome = (TextView)v.findViewById(R.id.welcome);
        mSuggestion = (TextView)v.findViewById(R.id.suggestion);
        mBackground = (ImageView)v.findViewById(R.id.image_background);

        SharedPreferences sharedPreferences;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(v.getContext());

        boolean isStudent;
        String studentName, yearsRemaining, homeWorld, index;

        // get preferences by key
        isStudent = sharedPreferences.getBoolean("Student", true);
        studentName = sharedPreferences.getString("Name", "unknown");
        studentName = getString(R.string.Hello, studentName);
        yearsRemaining = sharedPreferences.getString("Years", "0");
        index = sharedPreferences.getString("home_world", "0");

        mGreeting.setText(studentName);

        if(isStudent) {
            mBackground.setImageResource(images[Integer.parseInt(index)]);
            yearsRemaining = getString(R.string.wish_success, Integer.parseInt(yearsRemaining));
            mWelcome.setText(yearsRemaining);
        }
        else {
            mWelcome.setText(R.string.welcome);
            mBackground.setImageResource(R.drawable.academy);
        }

        return v;
    }


}
