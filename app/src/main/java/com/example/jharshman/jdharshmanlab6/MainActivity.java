package com.example.jharshman.jdharshmanlab6;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {


    private static final String SETTINGS = "SETTINGS";
    private static final String FIRST_USE = "FIRST_USE";

    private Fragment mMf;
    private Fragment mSf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(findViewById(R.id.frame_layout) != null) {
            if(savedInstanceState!=null) {
                return;
            }
            mMf = new MainFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.frame_layout, mMf)
                    .commit();
        }

        SharedPreferences preferences = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        boolean firstUse = preferences.getBoolean(FIRST_USE, true);
        if(firstUse) {
            mSf = new SettingsFragment();
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, mSf)
                    .addToBackStack(null)
                    .commit();

            preferences.edit().putBoolean(FIRST_USE, false).apply();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            mSf = new SettingsFragment();
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_layout, mSf)
                    .addToBackStack(null)
                    .commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }
}
